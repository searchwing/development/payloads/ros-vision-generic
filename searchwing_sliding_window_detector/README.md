
## Setup
* Download model from issue and place into assets folder here (better alternative: Use git-lfs in this repo and e.g. `git lfs pull`)
* Install onnx:
    * Most easy way: run `pip install onnxruntime`. Note: this will install onnx runtime without xnnpack execution provider. An a standard amd64 machine the difference is very small
    * Use on a rpi4 the prebuild wheel: `pip install assets/onnxruntime-1.16.0-cp38-cp38-linux_x86_64.whl`
    * OR : Build onnx runtime yourself with xnnpack support
       * Run `docker run -v $PWD/build_ort_xnnpack.sh:/tmp/build_ort_xnnpack.sh -v /tmp/ort:/tmp/ort ubuntu:jammy /bin/bash /tmp/build_ort_xnnpack.sh` This will create a onnx runtime xnnpack wheel in `/tmp/ort/Release/dist`



## Running tests
* `rostest searchwing_sliding_window_detector testSlidingWindowIntegration.test`
* `rostest searchwing_sliding_window_detector testSlidingWindowIntegrationLargeModel.test`   (note: path assumes that you are using the docker development, if not, change the file in the launch xml file)
* `rostest searchwing_sliding_window_detector testSlidingWindowUnit.test`


### Results
On a machine (ryzen 7) the both models easily pass the tests.

On a rp4 with on an ubuntu 20.04 ros docker image, onnxruntime 1.16.0, the execution time is:
* Small model:
    * ~ 1 sec for entire image processing (~ 0.93 sec for the detection part)
* Large model:
    * ~ 2.8 sec for entire image processing (~ 2.75 for the detection part)

Note: setting number of xnn threads ` <param name="modelWrapperParams" value='{"numThreadsXnn":2}' />` slows down the performance only little
Note: This is slightly different to a measurement on a ubuntu 22.04 with onnxruntime 1.15.1, where the exeuction time of the small model was approximately 13 ms. It is open where the additional ~7 ms come from. 
In addition, there is probably a small performance drop in rostest environment (but why)?s