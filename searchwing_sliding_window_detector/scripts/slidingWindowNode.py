

#!/usr/bin/env python

import time
from typing import NamedTuple
from pathlib import Path

# ros stuff
import rospy
from sensor_msgs.msg import Image
from vision_msgs.msg import (
    Detection2DArray,
    ObjectHypothesisWithPose,
    Detection2D,
    BoundingBox2D,
)


import cv2
cv2.useOptimized()
from cv_bridge import CvBridge
import numpy as np

from searchwing_sliding_window_detector.sliding_window_detector import (
    SlidingWindowDetector,
)
from searchwing_sliding_window_detector.onnx_model_wrapper import (
    get_default_model_file,
)


class InputParameter(NamedTuple):
    imgTopicName: str
    detectionTopicName: str
    modelFileName: Path
    subsamplingFactor: float
    windowSizeX: int
    windowSizeY: int
    threshold: float
    modelWrapperParams: str   # Parameters for the model wrapper provided as json string


class SWROSNode:
    DEFAULT_MODELFILENAME = get_default_model_file()
    DEFAULT_IMAGETOPICNAME = "/camera/image"
    DEFAULT_DETECTIONTOPICNAME = "/detection/slidingWindow"
    DEFAULT_SUBSAMPLINGFACTOR = 1
    DEFAULT_WINDOWSIZEX = 224
    DEFAULT_WINDOWSIZEY = 224
    DEFAULT_THRESHOLD = 0.05
    DEFAULT_MODELWRAPEPRPARAMS = ""

    def __init__(self) -> None:
        rospy.init_node("slidingWindowDetector")
        self._readInitParameters()
        self._initNode()

    def _readInitParameters(self) -> None:
        modelFileName = Path(
            rospy.get_param("~modelFileName", self.DEFAULT_MODELFILENAME)
        )
        rospy.logwarn(str(modelFileName))
        if not modelFileName.exists():
            raise ValueError("Model File Name must exists")
        self._inputParams = InputParameter(
            imgTopicName=rospy.get_param("~imgTopicName", self.DEFAULT_IMAGETOPICNAME),
            detectionTopicName=rospy.get_param(
                "~imgTopicName", self.DEFAULT_DETECTIONTOPICNAME
            ),
            modelFileName=modelFileName,
            subsamplingFactor=rospy.get_param(
                "~subsamplingFactor", self.DEFAULT_SUBSAMPLINGFACTOR
            ),
            windowSizeX=rospy.get_param(
                "~windowSizeX", self.DEFAULT_WINDOWSIZEX
            ),
            windowSizeY=rospy.get_param(
                "~windowSizeX", self.DEFAULT_WINDOWSIZEY
            ),
            threshold=rospy.get_param(
                "~threshold", self.DEFAULT_THRESHOLD
            ),
            modelWrapperParams=rospy.get_param(
                "~modelWrapperParams", self.DEFAULT_MODELWRAPEPRPARAMS
            ),
        )
        rospy.loginfo(f"Input Parameter Detected: {self._inputParams}")

    def _initNode(self):
        self.model = SlidingWindowDetector(
            self._inputParams.modelFileName,
            thresh=self._inputParams.threshold,
            sizeX=self._inputParams.windowSizeX,
            sizeY=self._inputParams.windowSizeY,
            wrapperParams=self._inputParams.modelWrapperParams
            )
        self.detectionsPub = rospy.Publisher(
            self._inputParams.detectionTopicName, Detection2DArray, queue_size=2
        )

    def startListening(self):
        rospy.loginfo("Start Listening")
        rospy.Subscriber(self._inputParams.imgTopicName, Image, self.callbackImg)
        rospy.logdebug("Subscription Started")
        rospy.spin()

    def callbackImg(self, data: Image):
        rospy.logdebug("Incoming Image")

        # Get Picture
        start = time.time()
        cv_bridge = CvBridge()
        cv_img: cv2 = cv_bridge.imgmsg_to_cv2(data, "rgb8")
        end = time.time()
        rospy.loginfo(f"convert pic with shape {cv_img.shape}[sec]: {end - start} ")

        # Run ROI-Detection Pipeline
        cv_img = self.preprocess_image(cv_img)
        end = time.time()
        rospy.loginfo(f"Interpolation {end - start} ")

        tilesWithObjectProposals = self.predict_objects(cv_img)
        end = time.time()
        rospy.loginfo(f"Detection {end - start} ")

        # Convert to ROS Types
        detectionsMsg = Detection2DArray()
        detectionsMsg.detections = [detection for detection in tilesWithObjectProposals]
        detectionsMsg.header.stamp = data.header.stamp

        end = time.time()
        rospy.loginfo(f"Sliding Window detection finished {end - start} ")
        rospy.logdebug(f"Number of detected Objects {len(tilesWithObjectProposals)}")

        self.detectionsPub.publish(detectionsMsg)

    def predict_objects(self, img: np.ndarray):
        return [
            self.convert_prediction_to_detection(i) for i in self.model.predict(img)
        ]

    def convert_prediction_to_detection(self, detection):
        bbox = BoundingBox2D()
        bbox.center.x = detection.bbox.x * self._inputParams.subsamplingFactor
        bbox.center.y = detection.bbox.y * self._inputParams.subsamplingFactor
        bbox.size_x = detection.bbox.size_x * self._inputParams.subsamplingFactor
        bbox.size_x = detection.bbox.size_x * self._inputParams.subsamplingFactor
        result = ObjectHypothesisWithPose(score=detection.score, id=detection.category)
        det = Detection2D(bbox=bbox, results=[result])
        return det

    def preprocess_image(self, img: np.ndarray) -> np.ndarray:
        if self._inputParams.subsamplingFactor == 1:
            return img
        out = cv2.resize(
            img,
            tuple([i // self._inputParams.subsamplingFactor for i in img.shape[0:2]]),
            interpolation=cv2.INTER_NEAREST,
        )
        return out


if __name__ == "__main__":
    try:
        swNode = SWROSNode()
        swNode.startListening()
    except rospy.ROSInterruptException:
        pass
