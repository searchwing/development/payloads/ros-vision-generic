from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["searchwing_sliding_window_detector"], package_dir={"": "src"}
)

setup(**d)
