#! /usr/bin/env python

import unittest
import rostest
import numpy as np

from searchwing_sliding_window_detector.sliding_window_utils import (
    get_indices_for_single_dimension,
    generate_sliding_window,
)


class SlidingWindowUtils(unittest.TestCase):
    def _testIndicesEqual(self, dim_length: int, window_size: int, result):
        self.assertTrue(
            np.all(get_indices_for_single_dimension(dim_length, window_size) == result)
        )

    def test_index_naive_method(self):
        # Note: in future different methods on how to calculate the indices could be possible
        # ToDo: Clarify whether window size includes the next element or not
        self._testIndicesEqual(10, 5, [0, 5])
        self._testIndicesEqual(10, 4, [0, 4, 6])
        self._testIndicesEqual(10, 3, [0, 3, 6, 7])

    def test_window_size_too_large(self):
        with self.assertRaises(ValueError):
            self._testIndicesEqual(10, 20, [])

    example_image = np.array(
        [
            [11, 21, 31, 41, 51],
            [12, 22, 32, 42, 52],
            [13, 23, 33, 43, 53],
            [14, 24, 34, 44, 54],
            [15, 25, 35, 45, 55],
        ]
    )

    def _testTileContentEqual(self, tile1: np.ndarray, tile2: np.ndarray):
        self.assertTrue(np.all(tile1 == tile2))

    def _testBoundingBoxEqual(self, tile, x, y, size_x, xize_y):
        self.assertEqual(tile.bbox.x, x)
        self.assertEqual(tile.bbox.y, y)
        self.assertEqual(tile.bbox.size_x, size_x)
        self.assertEqual(tile.bbox.size_y, xize_y)

    def test_single_tile(self):
        tiles = list(generate_sliding_window(self.example_image, (5, 5)))
        self.assertEqual(len(tiles), 1)
        tile = tiles[0]
        self._testTileContentEqual(tile.tile, self.example_image)
        self._testBoundingBoxEqual(tile, 2.5, 2.5, 5, 5)

    def test_multiple_tiles(self):
        tiles = list(generate_sliding_window(self.example_image, (2, 2)))
        self.assertEqual(len(tiles), 3 * 3)
        self._testTileContentEqual(tiles[0].tile, self.example_image[0:2, 0:2])
        self._testBoundingBoxEqual(tiles[0], 1, 1, 2, 2)
        self._testTileContentEqual(tiles[-1].tile, self.example_image[3:, 3:])
        self._testBoundingBoxEqual(tiles[-1], 4, 4, 2, 2)


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_sliding_window_detector",
        "test_sliding_window_indexer",
        SlidingWindowUtils,
    )