#! /usr/bin/env python

import json
import unittest
import rostest
import numpy as np
import time

from searchwing_sliding_window_detector.sliding_window_detector import (
    SlidingWindowDetector,
)
from searchwing_sliding_window_detector.onnx_model_wrapper import (
    get_default_model_file,
)


class SlidingWindowDetectorTest(unittest.TestCase):
    n_timing_executions = 3
    base_image_size = (3280, 2464, 3)

    @staticmethod
    def get_example_image(size):
        return np.random.randint(0, 256, size=size, dtype=np.uint8)

    def test_speed_prediction(self):
        detector = SlidingWindowDetector(get_default_model_file())

        example_image = self.get_example_image(
            (self.base_image_size[0] // 2, self.base_image_size[1] // 2, 3)
        )
        detector.predict(example_image)

        start = time.perf_counter()
        for i in range(self.n_timing_executions):
            detector.predict(example_image)
        elapsed = time.perf_counter() - start
        self.assertLessEqual(
            elapsed,
            self.n_timing_executions * 0.5,
            "Maximum exeuction time per Image (0.5 sec) exceeded"
            + f"({elapsed/self.n_timing_executions})",
        )

    def test_wrapper_model_parameters(self):
        # ToDo: We shouldn't have knowledge of the model! Should be private
        detector = SlidingWindowDetector(get_default_model_file())
        self.assertEqual(detector.model.numThreadsOnnx, 1)

        detector = SlidingWindowDetector(
            get_default_model_file(), wrapperParams=json.dumps({"numThreadsOnnx": 4}))
        self.assertEqual(detector.model.numThreadsOnnx, 4)


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_sliding_window_detector",
        "test_sliding_window_detector",
        SlidingWindowDetectorTest,
    )