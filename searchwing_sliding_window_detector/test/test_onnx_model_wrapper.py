#! /usr/bin/env python

import unittest
import rostest
import numpy as np
from pathlib import Path
import time

from searchwing_sliding_window_detector.onnx_model_wrapper import (
    OnnxModelWrapper,
    get_default_model_file,
)


class OnnxModelWrapperTest(unittest.TestCase):
    example_image = np.random.randint(256, size=(224, 224, 3), dtype=np.uint8)
    n_timing_executions = 50

    def test_default_model_file_exists(self):
        self.assertTrue(Path(get_default_model_file()).exists())

    def setUp(self) -> None:
        self.model = OnnxModelWrapper(get_default_model_file())

    def test_preprocessig_add_batch_on_demand(self):
        out = self.model.preprocess_tile(self.example_image)
        self.assertTupleEqual(out.shape, (1, 224, 224, 3))
        out = self.model.preprocess_tile(np.expand_dims(self.example_image, axis=0))
        self.assertTupleEqual(out.shape, (1, 224, 224, 3))

    def test_postprocess_single_output(self):
        id, score = self.model.postprocess_tile(np.asarray([[0.5]]))
        self.assertEqual(id, 1)
        self.assertEqual(score, 0.5)

    def test_exception_raised_for_multi_output(self):
        with self.assertRaises(AssertionError):
            self.model.postprocess_tile(np.asarray([[0.5, 0.5]]))

    def test_time_model_prediction(self):
        self.model.predict(self.example_image)

        start = time.perf_counter()
        for i in range(self.n_timing_executions):
            self.model.predict(self.example_image)
        elapsed = time.perf_counter() - start
        self.assertLessEqual(
            elapsed,
            self.n_timing_executions * 0.01,
            f"Maximum exeuction time per Image (0.01 sec) exceeded ({elapsed/self.n_timing_executions})",
        )

    def test_model_predict_handles_processing(self):
        id, score = self.model.predict(self.example_image)
        self.assertEqual(id, 1)
        self.assertGreaterEqual(score, 0)


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_sliding_window_detector",
        "test_onnx_model_wrapper",
        OnnxModelWrapperTest,
    )