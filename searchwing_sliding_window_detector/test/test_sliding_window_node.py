#!/usr/bin/env python

from time import time
import unittest
from pathlib import Path
import rospy

import cv2

from vision_msgs.msg import Detection2DArray
from sensor_msgs.msg import Image
import numpy as np


class SlidingWindowDetectionIntegrationTest(unittest.TestCase):
    DEFAULT_IMAGETOPICNAME = "/camera/image"
    DEFAULT_DETECTIONTOPICNAME = "/detection/slidingWindow"

    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)

        rospy.init_node("testSlidingWindowDetector")
        self.cameraPublisher = rospy.Publisher(
            self.DEFAULT_IMAGETOPICNAME, Image, queue_size=2
        )
        rospy.Subscriber(
            self.DEFAULT_DETECTIONTOPICNAME,
            Detection2DArray,
            self._onDetectionPublished,
        )

        # ToDo: Hack for waiting until the other node is up
        rospy.sleep(1)

    def _onDetectionPublished(self, data: Detection2DArray):
        self._data = data

    def _waitForDetection(self, timeout):
        """
        taken from wait_for_message, but we avoid creating the subscriber here
        """
        baseTime = time()
        timeout_t = baseTime + timeout
        while not rospy.core.is_shutdown() and self._data is None:
            rospy.rostime.wallsleep(0.01)
            if time() >= timeout_t:
                raise rospy.exceptions.ROSException(
                    "timeout exceeded while waiting for detection "
                )
        return time() - baseTime

    def _onPublishImage(self, img: np.ndarray):
        msg = Image()
        msg.header.stamp = rospy.Time.now()
        msg.height = img.shape[0]
        msg.width = img.shape[1]
        msg.encoding = "rgb8"
        msg.is_bigendian = False
        msg.step = 3 * msg.width
        msg.data = np.array(img).tobytes()
        self.cameraPublisher.publish(msg)
        rospy.logdebug("Image published")

    def _publishRandomImage(self, size=(224 * 2, 224 * 2, 3)):
        img = np.random.randint(0, 256, size=size)
        self._onPublishImage(img)

    def _publishImageFromFile(self, img_file: Path):
        img = cv2.imread(str(img_file))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self._onPublishImage(img)

    def setUp(self) -> None:
        # first call of the node is always slow, hence we publish an image once!
        self._data = None
        self._publishRandomImage(size=(2464, 3280, 3))
        self._waitForDetection(5)
        self._data = None

    def tearDown(self) -> None:
        self._data = None

    def test_speed_execution(self):
        """Test method description"""
        # Assume 8 MP camera
        self._publishRandomImage(size=(2464, 3280, 3))
        timeNeeded = self._waitForDetection(timeout=5)
        # ToDo: Clarify time
        self.assertLessEqual(
            timeNeeded,
            0.6,
            f"Maximum exeuction time per Image (0.6 sec) exceeded ({timeNeeded})",
        )

    def test_default_model_output(self):
        # Assume 8 MP camera
        self.assertTrue(self._data is None)
        self._publishImageFromFile(Path(Path(__file__).parent, "..", "assets/2021-10-01T11-25-52.543994.jpg"))
        self._waitForDetection(timeout=5)
        self.assertTrue(self._data is not None)

        # We do not care FP here, but the ship has to be detected! But, it should not be too much
        self.assertGreater(len(self._data.detections), 0)
        self.assertLess(len(self._data.detections), 10)

        # Check if boat is detected
        center_bounding_box_x = 448 / 2
        center_bounding_box_y = 448 + 448 / 2
        min_score = 0.9

        detection_found = False
        for detection in self._data.detections:
            if (detection.bbox.center.x == center_bounding_box_x and detection.bbox.center.y == center_bounding_box_y):
                self.assertTrue(detection.results[0].id == 1)
                self.assertTrue(detection.results[0].score > min_score)
                detection_found = detection.results[0].id == 1 and detection.results[0].score > min_score
        self.assertTrue(detection_found)


if __name__ == "__main__":
    import rostest

    rostest.rosrun(
        "searchwing_sliding_window_detector",
        "test_sliding_window_node",
        SlidingWindowDetectionIntegrationTest,
    )
