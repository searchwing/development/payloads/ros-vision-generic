import onnxruntime as ort
import numpy as np
from typing import Tuple
from pathlib import Path
import rospy


class OnnxModelWrapper:
    def __init__(self,
                 modelPath: str,
                 useXnn: bool = True,
                 numThreadsOnnx: int = 1,
                 numThreadsXnn: int = 4
                 ) -> None:
        # Note default values are suitable for xnnpack on rp4
        execution_providers = [
            ("CPUExecutionProvider", dict())
        ]

        if useXnn:
            if ("XnnpackExecutionProvider" not in ort.get_available_providers()):
                rospy.logwarn("Onnx runtime not build with execution provider. Please follow the help in the readme.")
            else:
                execution_providers.insert(
                    0, ("XnnpackExecutionProvider", {"intra_op_num_threads": numThreadsXnn})
                )
        options = ort.SessionOptions()
        options.intra_op_num_threads = numThreadsOnnx
        self.ort_session = ort.InferenceSession(
             str(modelPath), sess_options=options, providers=execution_providers)
        assert len(self.ort_session.get_inputs()) == 1
        assert len(self.ort_session.get_outputs()) == 1
        self.input_name = self.ort_session.get_inputs()[0].name
        self.ort_session.get_session_options()

    def __call__(self, inputData: np.ndarray) -> np.ndarray:
        out = self.ort_session.run(None, {self.input_name: inputData})[0]
        return out

    def predict(self, inputData: np.ndarray) -> np.ndarray:
        inputData = self.preprocess_tile(inputData)
        out = self(inputData)
        out = self.postprocess_tile(out)
        return out

    def preprocess_tile(self, inputData: np.ndarray) -> np.ndarray:
        # ToDo: further preprocessing here depending on the training!
        # We assume, that the batch-size is 1
        out = inputData
        if out.ndim == 3:
            out = np.expand_dims(out, axis=0)
        return out

    def postprocess_tile(self, inputData: np.ndarray) -> Tuple[int, float]:
        # ToDo: Score and ID calculation depends on training
        # We assume a single output here based on the mmpretrain configs
        assert inputData.size == 1
        score = inputData.flatten()[0]
        return 1, score

    @property
    def numThreadsOnnx(self):
        return self.ort_session.get_session_options().intra_op_num_threads


def get_default_model_file():
    base_path = Path(*Path(__file__).parts[:-3])
    return str(
        Path(
            base_path,
            "assets",
            "mobilenet_v3_small_e2e.onnx",
        )
    )