import numpy as np
from typing import Tuple, Generator
from dataclasses import dataclass


# ToDo: Check why I wasn't able to import BoundingBox2D from vision_msgs here
@dataclass
class BBox:
    x: float
    y: float
    size_x: float
    size_y: float


@dataclass
class Prediction:
    bbox: BBox
    category: int
    score: float


@dataclass
class ImageTile:
    bbox: BBox
    tile: np.array


def get_indices_for_single_dimension(dim_length: int, window_size: int):
    if dim_length < window_size:
        raise ValueError(
            f"Dimension size {dim_length} must be larger than window size {window_size}"
        )
    indices = np.arange(0, dim_length, window_size)
    indices[-1] = dim_length - window_size
    return indices


def generate_sliding_window(
    img: np.ndarray, window_size: Tuple[int, int]
) -> Generator[ImageTile, None, None]:
    # ToDo: Implement different methods for extracting the tiles, with correct border handling
    x_indices = get_indices_for_single_dimension(img.shape[1], window_size[1])
    y_indices = get_indices_for_single_dimension(img.shape[0], window_size[0])
    for x_index in x_indices:
        for y_index in y_indices:
            box = BBox(
                x=x_index + float(window_size[0]) / 2,
                y=y_index + float(window_size[1]) / 2,
                size_x=window_size[0],
                size_y=window_size[1],
            )
            if img.ndim == 3:
                tile = img[
                    y_index : y_index + window_size[1],
                    x_index : x_index + window_size[0],
                    :,
                ]
            elif img.ndim == 2:
                tile = img[
                    y_index : y_index + window_size[1],
                    x_index : x_index + window_size[0]
                ]
            else:
                raise ValueError(
                    "Image should have either 2 or 3 dimensions (with channels last)"
                )
            yield ImageTile(bbox=box, tile=tile)
