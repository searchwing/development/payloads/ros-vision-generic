apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    build-essential \
    git \
    python3-numpy

pip3 install cmake packaging

cd /tmp/onnx_runtime && \
    git clone --recursive https://github.com/Microsoft/onnxruntime.git && \
    cd onnxruntime && \
    git checkout tags/v1.16.0 -b test && \
    ./build.sh --config Release --build_shared_lib --parallel --enable_pybind --skip_tests --build_wheel  --update --build --allow_running_as_root --use_xnnpack
    cp -r build/Linux/Release/ /tmp/ort
    rm -r /tmp/onnxruntime