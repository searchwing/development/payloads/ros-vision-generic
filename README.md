#ROS code for image taking and object detection  
- more information in ros-generic README.md 

##Structure
- ros-vision-generic
  - Nodes
    - Detektors
      - TensorRT specifics like searchwing_retinanet
      - TBA Sliding window based MobileNet/XNNPack object detection
    - Sensors
      - Pi camera specific image creators/driver
  - Launchfiles
    - Generic vision testing launchfiles
  

