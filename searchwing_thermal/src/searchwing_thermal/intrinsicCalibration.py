import numpy as np
import cv2
import glob
from tqdm import tqdm
from .blob_detector import blob_detector

#Calib Circle Grid Setting
gridPointsWidth = 9
gridPointsHeight = 7
gridPointsDist = 0.02 #[m]
scale = 3

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((gridPointsHeight*gridPointsWidth,3), np.float32)
objp[:,:2] = np.mgrid[0:gridPointsWidth,0:gridPointsHeight].T.reshape(-1,2)*gridPointsDist 

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('/home/julle/ControlerProjekte/SearchWing/Hardware/FLIR/Calibration/20200731/2/good/*.png')

for fname in tqdm(images):
    img_raw = cv2.imread(fname, -1)
    img_raw = cv2.resize(img_raw,None,fx=scale,fy=scale)

    gray =  (img_raw/256).astype('uint8')  #cv2.cvtColor(img_raw, cv2.COLOR_BGR2GRAY)

    gray = cv2.bitwise_not(gray)

    # Find the chess board corners
    ret,centers=cv2.findCirclesGrid(gray, (gridPointsWidth,gridPointsHeight),cv2.CALIB_CB_SYMMETRIC_GRID)
    print(ret)
    # If found, add object points, image points 
    if ret == True:
        objpoints.append(objp)
        imgpoints.append(centers)

        # Draw and display the corners
        img_circles = cv2.drawChessboardCorners(img_raw, (gridPointsWidth,gridPointsHeight), centers,ret)

        #cv2.imshow('img_circles',img_circles)
        #cv2.waitKey(0)

cv2.destroyAllWindows()

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

mtx = mtx/scale
mtx[2][2]=1
#dist = dist / scale
print("error: ", ret)
print("Projection Mat: ", mtx)
print("distortion Coeffs: ", dist)

#test undistortion

for fname in tqdm(sorted(images)):
    img_raw = cv2.imread(fname, -1)

    img_raw =  (img_raw/256).astype('uint8')  #cv2.cvtColor(img_raw, cv2.COLOR_BGR2GRAY)
 
    h,  w = img_raw.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
    # undistort
    dst = cv2.undistort(img_raw, mtx, dist, None, newcameramtx)
    dst = cv2.resize(img_raw,None,fx=4,fy=4)

    cv2.imshow('dst',dst)
    cv2.waitKey(0)