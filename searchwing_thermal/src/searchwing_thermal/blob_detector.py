#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
from .normalizer import normalize
from .flatFieldCorrection import flatFieldCorrection

class blob_detector_SimpleBlobDetector():
    def __init__(self):
        self.thermalTreshPercentile = 0.995
        self.pixelOffsetWindowInnerImage = 10

    def createBlobDetector(self,minTresh):
        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()
        # Change thresholds
        params.minThreshold = minTresh
        params.maxThreshold = 255
        # Color
        params.filterByColor = True
        params.blobColor = 255
        # Filter by Area.
        params.filterByArea = True
        params.minArea = 1
        params.maxArea = 60
        # Filter by Circularity
        params.filterByCircularity = False
        params.minCircularity = 0.1
        # Filter by Convexity
        params.filterByConvexity = False
        params.minConvexity = 0.87
        # Filter by Inertia
        params.filterByInertia = False
        params.minInertiaRatio = 0.01
        # Create a detector with the parameters
        detector = cv2.SimpleBlobDetector_create(params)
        return detector

    def detect(self,img):
        thresh = self.calcTreshold(img,self.pixelOffsetWindowInnerImage,self.thermalTreshPercentile)

        detector = self.createBlobDetector(thresh)
        # Detect blobs.
        keypoints = detector.detect(img)

        # Draw detected blobs as red circles.
        # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
        im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (255,0,0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        return im_with_keypoints


class blob_detector():
    def __init__(self, debug = False, normalize = False, thermalTreshPercentile = 0.5,peaksFilterAreaPixelCntMax=5, peaksDilate = 6, pixelOffsetWindowInnerImage=7,cropPixelTopBottom=0,cropPixelLeftRight=0):
        self.thermalTreshPercentile = thermalTreshPercentile
        self.pixelOffsetWindowInnerImage = pixelOffsetWindowInnerImage
        self.peaksFilterAreaPixelCntMax = peaksFilterAreaPixelCntMax
        self.peaksDilate = peaksDilate
        self.debug=debug
        self.normalize=normalize
        self.cropPixelTopBottom = cropPixelTopBottom
        self.cropPixelLeftRight = cropPixelLeftRight
        self.ffc = flatFieldCorrection(15,3)

    def detect(self,thermalImg):
        if self.normalize :
            thermalImgNorm=normalize(thermalImg,self.ffc.getFlatFieldCorrection(thermalImg),cropPixelTopBottom=self.cropPixelTopBottom,cropPixelLeftRight=self.cropPixelLeftRight)
        else:
            thermalImgNorm=thermalImg
        #self.thermalImgNorm = thermalImgNorm 

        if self.debug : cv2.imshow("thermalImgNorm ",cv2.resize(thermalImgNorm,None,fx=4,fy=4))
        #if self.debug : cv2.imshow("thermalImgNormInv ",cv2.resize(np.invert(thermalImgNorm),None,fx=4,fy=4))  

        #detect on normal image for warm spots
        img_norm_peaks_area_filtered_threshed_dilated_1 = self.detect_peaks(thermalImgNorm)


        #detect on inverted image for cold spots
        inv_img = np.zeros(thermalImg.shape, np.uint16)
        inv_img_full = np.invert(thermalImgNorm)
        inv_img[0+self.cropPixelTopBottom:inv_img.shape[0]-self.cropPixelTopBottom,0+self.cropPixelLeftRight:inv_img.shape[1]-self.cropPixelLeftRight] = inv_img_full[0+self.cropPixelTopBottom:inv_img.shape[0]-self.cropPixelTopBottom,0+self.cropPixelLeftRight:inv_img.shape[1]-self.cropPixelLeftRight]
        if self.debug : cv2.imshow("thermalImgNormInv ",cv2.resize(inv_img,None,fx=4,fy=4))     
        img_norm_peaks_area_filtered_threshed_dilated_2 = self.detect_peaks(inv_img)   
        #if self.debug : cv2.waitKey()

        img_norm_peaks_area_filtered_threshed_dilated_combined = img_norm_peaks_area_filtered_threshed_dilated_1 | img_norm_peaks_area_filtered_threshed_dilated_2
        if self.debug : cv2.imshow("img_norm_peaks_area_filtered_threshed_dilated_combined ",cv2.resize(img_norm_peaks_area_filtered_threshed_dilated_combined,None,fx=4,fy=4))
        
        img_norm_peaks_area_filtered_threshed_dilated_combined = img_norm_peaks_area_filtered_threshed_dilated_combined.astype(np.uint8)
        #get centers of dilated objects
        centers=[]
        contours, hierarchy = cv2.findContours(img_norm_peaks_area_filtered_threshed_dilated_combined, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for oneContour in contours:
            pos = np.mean(oneContour,axis=0)
            #kp = cv2.KeyPoint(x=pos[0][0],y=pos[0][1],_size=len(oneContour)+1)
            centers.append((int(pos[0][0]),int(pos[0][1])))
 
 
        #draw centers   
        if self.debug : 
            centers_img = np.zeros(thermalImgNorm.shape,np.uint8)
            for oneCenter in centers:
                if oneCenter is None or len(oneCenter) == 0:
                    continue
                centers_img[oneCenter[1],oneCenter[0]]=255
            cv2.imshow("centers_img ", cv2.resize(centers_img,None,fx=4,fy=4))    
            cv2.waitKey()
        return centers, thermalImgNorm
     
    def detect_peaks(self,thermalImgNorm):

        #filter for Peaks (from https://stackoverflow.com/questions/19122690/fast-peak-finding-and-centroiding-in-python)
        kernelsize=5
        kernel = np.ones((kernelsize, kernelsize), np.uint8) 
        img_dilated = cv2.dilate(thermalImgNorm, kernel, iterations=1)
        img_dilated_same1 = np.uint8((thermalImgNorm == img_dilated)*255)
        if self.debug : cv2.imshow("img_dilated1 ",cv2.resize(img_dilated,None,fx=4,fy=4))
        if self.debug : cv2.imshow("img_dilated_same1 ",cv2.resize(img_dilated_same1,None,fx=4,fy=4))

        kernelsize=7
        kernel = np.ones((kernelsize, kernelsize), np.uint8) 
        img_dilated = cv2.dilate(img_dilated, kernel, iterations=1)
        img_dilated_same2 = np.uint8((thermalImgNorm == img_dilated)*255)
        img_norm_peaks = np.uint8((thermalImgNorm == img_dilated)*thermalImgNorm)
        if self.debug : cv2.imshow("img_norm_peaks ", cv2.resize(img_norm_peaks,None,fx=4,fy=4))


        #filter out huge patches in peaks image
        contours, hierarchy = cv2.findContours(img_norm_peaks, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contoursFiltered = []
        img_norm_peaks_area_filtered_binary = np.zeros(img_norm_peaks.shape,np.uint8)
        for oneContour in contours:
            area = len(oneContour)
            if area > self.peaksFilterAreaPixelCntMax:
                continue
            a=oneContour.reshape([len(oneContour),2])
            img_norm_peaks_area_filtered_binary[a[:,1],a[:,0]]=1
            
        if self.debug : cv2.imshow("img_norm_peaks_area_filtered_binary ", cv2.resize(img_norm_peaks_area_filtered_binary*255,None,fx=4,fy=4))
        img_norm_peaks_area_filtered = img_norm_peaks_area_filtered_binary*thermalImgNorm
        if self.debug : cv2.imshow("img_norm_peaks_area_filtered ", cv2.resize(img_norm_peaks_area_filtered,None,fx=4,fy=4))

        
        #treshold Peaks
        percentile = self.thermalTreshPercentile
        pixelOffsetWindowInnerImage=self.pixelOffsetWindowInnerImage
        imgSize = img_norm_peaks_area_filtered.shape
        windowed_img_norm_peaks_area_filtered= img_norm_peaks_area_filtered[pixelOffsetWindowInnerImage:imgSize[0]-pixelOffsetWindowInnerImage,pixelOffsetWindowInnerImage:imgSize[1]-pixelOffsetWindowInnerImage]
        peaks_values = windowed_img_norm_peaks_area_filtered[windowed_img_norm_peaks_area_filtered > 0]
        maxPossiblePixelVal = np.iinfo(img_norm_peaks_area_filtered.dtype).max
        if len(peaks_values) > 0:
            peaks_thresh = np.percentile(peaks_values, percentile*100)
        else:
            peaks_thresh = maxPossiblePixelVal
        _,img_norm_peaks_area_filtered_threshed = cv2.threshold(img_norm_peaks_area_filtered, peaks_thresh, maxPossiblePixelVal, cv2.THRESH_BINARY)
 
        if self.debug : cv2.imshow("img_norm_peaks_area_filtered_threshed ", cv2.resize(img_norm_peaks_area_filtered_threshed,None,fx=4,fy=4))

        #dilate to get less objects
        kernelsize=self.peaksDilate 
        kernel = np.ones((kernelsize, kernelsize), np.uint8) 
        img_norm_peaks_area_filtered_threshed_dilated = cv2.dilate(img_norm_peaks_area_filtered_threshed, kernel, iterations=1)
        if self.debug : cv2.imshow("img_norm_peaks_area_filtered_threshed_dilated ", cv2.resize(img_norm_peaks_area_filtered_threshed_dilated,None,fx=4,fy=4))
        return img_norm_peaks_area_filtered_threshed_dilated


def drawDetections(img,detections):
    img_8 = (img.copy()/255).astype(np.uint8)
    img_rgb = cv2.cvtColor(img_8,cv2.COLOR_GRAY2RGB)
    for oneDet in detections:
        #Python: cv.Circle(img, center, radius, color, thickness=1, lineType=8, shift=0) → None
        center = (oneDet[0],oneDet[1])
        cv2.circle(img_rgb ,center,radius=3,color=(255,0,0))
    return img_rgb

##TESTING ON DATA
if __name__ == '__main__':
    from common import *

    detector = blob_detector(
        debug=True,
        normalize=True,
        thermalTreshPercentile=0.999  ,
        peaksFilterAreaPixelCntMax=50,
        peaksDilate=10,
        pixelOffsetWindowInnerImage=1,
        cropPixelTopBottom=10,cropPixelLeftRight=10)
        

    #imPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200712_phillipRosFlirErstFlug/flug2/data/bilder/thermal/2020-07-10T04-46-19.944.png"
    thermalImgPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200725_fahrlandersee/3_zechliner_see/data/bilder/thermal/2020-07-25T18-21-12.398.png"
    thermalImg = os.path.basename(thermalImgPath)
    thermalFolder = os.path.dirname(thermalImgPath)

    allThermal = sorted(glob(os.path.join(thermalFolder,"*.png")))

    found = False
    for oneThermal in allThermal :
        if found == False and thermalImg in oneThermal:
            found = True
        if found == True:
            
            thermalImg = cv2.imread(oneThermal,-1)
            #thermalImg = normalize(thermalImg,cropPixelTopBottom=0,cropPixelLeftRight=0)
            det = detector.detect(thermalImg)

            detImg = drawDetections(thermalImg,det)
            cv2.imshow("detImg ",cv2.resize(detImg ,None,fx=4,fy=4))
            cv2.waitKey(0)

            normalPath = find_next_normal_img(oneThermal )
            normalImg = cv2.imread(normalPath,cv2.IMREAD_COLOR)
            if normalImg is None:
                continue
            cv2.imshow("normalImg ",cv2.resize(normalImg ,None,fx=0.2,fy=0.2))
            cv2.waitKey(0)
