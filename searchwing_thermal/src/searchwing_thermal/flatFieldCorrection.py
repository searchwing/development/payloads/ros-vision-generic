#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
from glob import glob
import os

class flatFieldCorrection():
    def __init__(self, bufferSize, bufferUpdateImageSkipCount):
        self.bufferSize = bufferSize
        self.buffer = []
        self.bufferUpdateImageSkipCount = bufferUpdateImageSkipCount
        self.bufferUpdateImageSkipCounter = 0

    def getFlatFieldCorrection(self, img):
        #if buffer is empty add at least one image
        if len(self.buffer) == 0:
            self.buffer.append(img)

        #check if buffer should be updated
        self.bufferUpdateImageSkipCounter += 1
        if self.bufferUpdateImageSkipCounter == self.bufferUpdateImageSkipCount:
            self.bufferUpdateImageSkipCounter = 0

            self.buffer.append(img)
            if len(self.buffer)>self.bufferSize:
                self.buffer.pop(0)

        #do actual ffc : calc median per pixel for buffer of images
        return np.median(np.dstack(self.buffer), axis=-1).astype(np.uint16)

if __name__ == "__main__":
    from normalizer import normalize

    files = sorted(glob("/run/media/julle/seagate/data/flightdata/2020-09-11_wismar/3/bilder/thermal/*.png"))
    files = files[138:]

    ffc = flatFieldCorrection(15,3)

    for oneFile in files:
        img = cv2.imread(oneFile, -1) # -1 == leave format as it is.
        if img is None:
            continue
        print("std: ",np.std(img))
        print("max: ",np.max(img))
        normImg1 = normalize(img,ffc.getFlatFieldCorrection(img),cropPixelTopBottom=10,cropPixelLeftRight=10)
        cv2.imshow("ffcnormed", cv2.resize(normImg1,None,fx=4,fy=4))

        normImg2 = normalize(img,None,cropPixelTopBottom=10,cropPixelLeftRight=10)
        cv2.imshow("normed", cv2.resize(normImg2,None,fx=4,fy=4))
        cv2.waitKey(0)
