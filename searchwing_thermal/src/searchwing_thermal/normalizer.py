#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
from glob import glob
import os

def normalize(img, ffcImg, cropPixelTopBottom = 0,cropPixelLeftRight=0):
    tmp = img.copy()
    if not(ffcImg is None):
        ffcNormalized = tmp.astype(int) - ffcImg # normalize using ffc image (signed)
        ffcNormalizedZeroBased = ffcNormalized - np.min(tmp) # zero base all values
        tmp = ffcNormalizedZeroBased.astype(np.uint16) # convert back to unsigned
    
    maxVal = np.iinfo(tmp.dtype).max
    if cropPixelTopBottom > 0 or cropPixelLeftRight>0:
        zeros = np.zeros(tmp.shape, np.uint16)
        cutOut = tmp[0+cropPixelTopBottom:tmp.shape[0]-cropPixelTopBottom,0+cropPixelLeftRight:tmp.shape[1]-cropPixelLeftRight]
        cv2.normalize(cutOut , cutOut,0, maxVal, cv2.NORM_MINMAX)
        zeros[0+cropPixelTopBottom:tmp.shape[0]-cropPixelTopBottom,0+cropPixelLeftRight:tmp.shape[1]-cropPixelLeftRight]=cutOut
        tmp = zeros 
    else:
        cv2.normalize(tmp , tmp,0, maxVal, cv2.NORM_MINMAX)

    return tmp

if __name__ == "__main__":

    files = sorted(glob("/run/media/julle/seagate/data/flightdata/2020-09-11_wismar/3/bilder/thermal/*.png"))
    files = files[18:]

    for oneFile in files:
        img = cv2.imread(oneFile, -1) # -1 == leave format as it is.
        if img is None:
            continue
        normImg2 = normalize(img,None,cropPixelTopBottom=10,cropPixelLeftRight=10)
        cv2.imshow("normed", cv2.resize(normImg2,None,fx=4,fy=4))
        cv2.waitKey(0)
