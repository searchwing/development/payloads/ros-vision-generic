import os
from glob import glob
from datetime import datetime

def get_stamp_from_filename(filename, extension):
    return datetime.strptime(filename, "%Y-%m-%dT%H-%M-%S.%f" + extension)

def find_next_normal_img(thermalImgPath):
    thermalFolder = os.path.abspath(thermalImgPath)
    thermalImgName = os.path.basename(thermalImgPath)
    thermalStamp = get_stamp_from_filename(thermalImgName,".png")

    NormalFolder = os.path.dirname(thermalImgPath)[:-7]+"normal/"
    NormalImages = sorted(glob(os.path.join(NormalFolder,"*.jpg")))
    
    biggerFoundIdx = -1
    for idx,oneNormal in enumerate(NormalImages) :
        normalStamp = get_stamp_from_filename(os.path.basename(oneNormal),".jpg")
        if normalStamp >thermalStamp:
            biggerFoundIdx= idx
            break
    stamp0 = get_stamp_from_filename(os.path.basename(NormalImages[biggerFoundIdx-1]),".jpg")
    stamp1 = get_stamp_from_filename(os.path.basename(NormalImages[biggerFoundIdx]),".jpg")
    dt0 = thermalStamp-stamp0 
    dt1 = stamp1-thermalStamp
    if dt1 > dt0:
        return NormalImages[biggerFoundIdx-1]
    else:
        return NormalImages[biggerFoundIdx]