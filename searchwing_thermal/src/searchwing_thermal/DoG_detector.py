#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
from .normalizer import normalize

class DoG_detector():
    def __init__(self):
        self.dog_big_kernel = 9
        self.dog_small_kernel = 3
        self.debug = True

    def detect(self, img):
        # Get params
        big_kernel = self.dog_big_kernel
        small_kernel = self.dog_small_kernel

        # Get gray roi
        #gray_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)

        ## Calculate a mean in the vertical direction
        #roi_mean = np.mean(gray_roi, axis=0).astype(np.uint8).reshape(1,roi.shape[1])

        ## Make fft  (not used currently) 
        #f = np.fft.fft2(roi_mean)
        #fshift = np.fft.fftshift(f)
        #magnitude_spectrum = (20*np.log(np.abs(fshift))).astype(np.uint8)


        # Calculate difference of gaussians
        blur_large = cv2.blur(img, (big_kernel,big_kernel)).astype(np.float)
        blur_small = cv2.blur(img, (small_kernel,small_kernel)).astype(np.float)
        dog = blur_small - blur_large

        # Ensure all values are above 0
        dog[dog < 0] = 0

        # Scale image to uint8 scale
        dog = dog * 255

        # Convert image
        dog = dog.astype(np.uint8)

        # Show debug images
        if self.debug:
            cv2.imshow('blur_large', blur_large)
            cv2.imshow('blur_small', blur_small)
            cv2.imshow('DoG', cv2.resize(dog ,None,fx=4,fy=4))
            #cv2.imshow('SPECTRUM', magnitude_spectrum)

        return dog



 

##TESTING ON DATA
if __name__ == '__main__':
    from common import *
    import os

    detector = DoG_detector()

    #imPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200712_phillipRosFlirErstFlug/flug2/data/bilder/thermal/2020-07-10T04-46-19.944.png"
    thermalImgPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200712_phillipRosFlirErstFlug/flug2/data/bilder/thermal/2020-07-10T04-38-12.911.png"
    thermalImg = os.path.basename(thermalImgPath)
    thermalFolder = os.path.dirname(thermalImgPath)

    allThermal = sorted(glob(os.path.join(thermalFolder,"*.png")))

    found = False
    for oneThermal in allThermal :
        if found == False and thermalImg in oneThermal:
            found = True
        if found == True:
            
            thermalImg = cv2.imread(oneThermal,cv2.IMREAD_GRAYSCALE)
            cv2.imshow("thermalImg ",cv2.resize(thermalImg ,None,fx=4,fy=4))

            detector.detect(thermalImg)

            normalized = normalize(thermalImg)
            cv2.imshow("normalized ",cv2.resize(normalized ,None,fx=4,fy=4))
            
            
            normalPath = find_next_normal_img(oneThermal )
            normalImg = cv2.imread(normalPath,cv2.IMREAD_COLOR)
            cv2.imshow("normalImg ",cv2.resize(normalImg ,None,fx=0.5,fy=0.5))
            cv2.waitKey(0)
