#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
import os

import rospy
import tf
import message_filters

from sensor_msgs.msg import Image, CompressedImage

from cv_bridge import CvBridge
from searchwing_thermal.normalizer import normalize
from searchwing_thermal.flatFieldCorrection import flatFieldCorrection
cvbridge = CvBridge()


def ros2opencv(img):
    cv_img = cvbridge.imgmsg_to_cv2(img, "mono16")
    return cv_img

def opencv2ros(img):
    ros_img = cvbridge.cv2_to_imgmsg(img, "mono16")
    return ros_img


def callbackImage(rosImgMsg ): #GPS
    cvImg = ros2opencv(rosImgMsg )
    imgMin = np.min(cvImg)
    imgMax = np.max(cvImg)
    rospy.loginfo("Std Dev:"+ str(np.std(cvImg))+ "\tmin: " + str(imgMin) + "\tmax: " + str(imgMax)+ "\tdelta: " + str(imgMax-imgMin))
    
    cvImg = normalize(cvImg,ffc.getFlatFieldCorrection(cvImg),cropPixelTopBottom=0,cropPixelLeftRight=0)
    rosNormImgMsg = opencv2ros(cvImg)
    rosNormImgMsg.header.stamp = rosImgMsg.header.stamp
    normImg_pub.publish(rosNormImgMsg)

# start node
rospy.init_node('normalizer_node', anonymous=True)

# subscriber
imgTopicName  = rospy.get_param('~i_imgTopicName',"/thermal/image")
rospy.Subscriber(imgTopicName ,Image,callbackImage)

ffcBufferSize  = rospy.get_param('~i_ffcBufferSize',15)
ffcBufferUpdateImageSkipCount  = rospy.get_param('~i_ffcBufferUpdateImageSkipCount',3)

ffc = flatFieldCorrection(ffcBufferSize,ffcBufferUpdateImageSkipCount)

#publisher
normImg_pub = rospy.Publisher('/thermal/normalized', Image, queue_size=3)

print("node started: Loop until new data arrives")
rospy.spin()


