#!/usr/bin/env python
 
import cv2
cv2.useOptimized()
import numpy as np
import os

import rospy
import tf
import message_filters

from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
from sensor_msgs.msg import Image, CompressedImage

from searchwing_thermal.blob_detector import blob_detector

from cv_bridge import CvBridge
cvbridge = CvBridge()



def ros2opencv(img):
    cv_img_rgb = cvbridge.imgmsg_to_cv2(img, "mono16")
    return cv_img_rgb

def opencv2ros(img,enc):
    ros_img = cvbridge.cv2_to_imgmsg(img, enc)
    return ros_img

def drawDetections(img,detections):
    img_8 = (img.copy()/255).astype(np.uint8)
    img_rgb = cv2.cvtColor(img_8,cv2.COLOR_GRAY2RGB)
    for oneDet in detections:
        #Python: cv.Circle(img, center, radius, color, thickness=1, lineType=8, shift=0) → None
        center = (oneDet[0],oneDet[1])
        cv2.circle(img_rgb ,center,radius=3,color=(255,0,0))
    return img_rgb

def callbackImage(rosImgMsg ):
    cvImg = ros2opencv(rosImgMsg) 

    # Detect blobs (+ get normalized img)
    thermalDetections, thermalImgNorm = detector.detect(cvImg)

    #Convert to ROS Types
    detections=[]
    for oneDetections in thermalDetections :
        detection = Detection2D()
        box = BoundingBox2D()
        box.center.x = oneDetections[0]
        box.center.y = oneDetections[1]
        box.size_x = 10
        box.size_y = 10
        detection.bbox = box
        detections.append(detection)
    detectionsMsg = Detection2DArray()
    detectionsMsg.detections = detections
    detectionsMsg.header.stamp = rosImgMsg.header.stamp
        
    detectionsPub.publish(detectionsMsg)

    #Draw
    cvImgWithDetections = drawDetections(thermalImgNorm,thermalDetections)
    rosBlobImgMsg = opencv2ros(cvImgWithDetections,"rgb8")
    rosBlobImgMsg.header.stamp = rosImgMsg.header.stamp
    blobImg_pub.publish(rosBlobImgMsg)


# start node
rospy.init_node('blob_detector_node', anonymous=True)

thermalTreshPercentile  = rospy.get_param('~i_thermalTreshPercentile',0.99)
peaksFilterAreaPixelCntMax  = rospy.get_param('~i_peaksFilterAreaPixelCntMax',50)
peaksDilate  = rospy.get_param('~i_peaksDilate',10)


detector = blob_detector(
    debug=False,
    normalize=False,
    thermalTreshPercentile=thermalTreshPercentile,
    peaksFilterAreaPixelCntMax=peaksFilterAreaPixelCntMax,
    peaksDilate=peaksDilate,
    pixelOffsetWindowInnerImage=1,
    cropPixelTopBottom=0,
    cropPixelLeftRight=0)
    
# subscriber
imgTopicName  = rospy.get_param('~i_imgTopicName',"/thermal/image")
rospy.Subscriber(imgTopicName ,Image,callbackImage)

#publisher
blobImg_pub = rospy.Publisher('/thermal/blob_image', Image, queue_size=3)
detectionsPub = rospy.Publisher('/detections/Thermal', Detection2DArray, queue_size=2)


print("node started: Loop until new data arrives")
rospy.spin()


