#!/usr/bin/env python
 
import os
import io
from threading import Event
from time import sleep
from datetime import datetime

import rospy
from sensor_msgs.msg import Image, CompressedImage
from searchwing_common_py import camCalibProvider

from picamera import mmal, mmalobj as mo

import numpy as np

# start node

nodeName = 'pi_cam_dual_resolution_node'+str(np.random.randint(100))
print(nodeName)
rospy.init_node(nodeName, anonymous=True)

i_camName  = rospy.get_param('~i_camName',"pi_cam_dual_resolution_node")
i_camPort  = rospy.get_param('~i_camPort',0)
print("i_camPort:",i_camPort)
i_camInfoUrl = rospy.get_param('~camera_info_url',"package://searchwing_common_py/config/bodenseeCamCalib.yaml")

i_imgPeriodTime  = rospy.get_param('~i_imgPeriodTime',2.0)
i_captureMode = rospy.get_param('~i_captureMode',"port")

print(i_captureMode)

#publisher
Img_full_pub = rospy.Publisher('/'+i_camName+'/full', Image, queue_size=3)
Img_half_pub = rospy.Publisher('/'+i_camName+'/half', Image, queue_size=3)
camera_info_pub = rospy.Publisher('/'+i_camName+'/camera_info', Image, queue_size=3)

finished_full = Event()
finished_half = Event()

# from https://picamera.readthedocs.io/en/release-1.13/api_mmalobj.html#threads-synchronization
def image_encoder_full_file_callback(port, buf):
    jpg_file_full.write(buf.data)
    #print(buf)
    if buf.flags & mmal.MMAL_BUFFER_HEADER_FLAG_FRAME_END:
        finished_full.set()
        return True
    return False

def image_encoder_half_file_callback(port, buf):
    jpg_file_half.write(buf.data)
    if buf.flags & mmal.MMAL_BUFFER_HEADER_FLAG_FRAME_END:
        finished_half.set()
        return True
    return False


def image_encoder_full_port_callback(port, buf):
    if buf.flags & mmal.MMAL_BUFFER_HEADER_FLAG_FRAME_END:
        picMsg = CompressedImage()
        picMsg.header.stamp = imageStamp
        picMsg.format = "jpg"
        picMsg.data = buf.data
        Img_full_pub.publish(picMsg)

        finished_full.set()
        return True
    return False

def image_encoder_half_port_callback(port, buf):
    if buf.flags & mmal.MMAL_BUFFER_HEADER_FLAG_FRAME_END:
        picMsg = CompressedImage()
        picMsg.header.stamp = imageStamp
        picMsg.format = "jpg"
        picMsg.data = buf.data
        Img_half_pub.publish(picMsg)

        finished_half.set()
        return True
    return False

def capture_to_disk(filename="aaa", path=""):
    global jpg_file_full
    global jpg_file_half
    
    picStamp = datetime.now()
    filename = picStamp.strftime("%Y-%m-%dT%H-%M-%S.%f") + ".jpg"
    jpg_file_full = io.open(os.path.join(path,'F_'+filename+'.jpg'), 'wb')
    #jpg_file_half = io.open(os.path.join(path,'H_'+filename+'.jpg'), 'wb')
    finished_full.clear()
    #finished_half.clear()

    #encoder.outputs[0].enable(image_callback)
    camera.outputs[2].params[mmal.MMAL_PARAMETER_CAPTURE] = True
    imageStamp = rospy.Time.now()
    if not (finished_full.wait(4)):# and finished_half.wait(4)):
        raise Exception('capture timed out')
    camera.outputs[2].params[mmal.MMAL_PARAMETER_CAPTURE] = False
    print(filename)
    #encoder.outputs[0].disable()
    jpg_file_full.close()
    #jpg_file_half.close()

def capture_to_port():
    global imageStamp

    finished_full.clear()
    finished_half.clear()

    #encoder.outputs[0].enable(image_callback)
    camera.outputs[2].params[mmal.MMAL_PARAMETER_CAPTURE] = True
    imageStamp = rospy.Time.now()
    if not (finished_full.wait(4) and finished_half.wait(4)):
        raise Exception('capture timed out')
    camera.outputs[2].params[mmal.MMAL_PARAMETER_CAPTURE] = False
    #encoder.outputs[0].disable()

def image_capture_callback(port, buf):
    rospy.loginfo("capture done!")



FULLSIZE_RES = (3280, 2464)
HALFSIZE_RES = (1640, 1232)

#from https://github.com/waveform80/picamera/blob/7e4f1d379d698c44501fb84b886fadf3fc164b70/picamera/camera.py#L617
try:
    camera = mo.MMALCamera()
except PiCameraMMALError as e:
    if e.status == mmal.MMAL_ENOMEM:
        raise PiCameraError(
            "Camera is not enabled. Try running 'sudo raspi-config' "
            "and ensure that the camera has been enabled.")
    else:
        raise
#https://picamera.readthedocs.io/en/release-1.13/api_mmalobj.html#file-input-jpeg-encoding
camera.control.params[mmal.MMAL_PARAMETER_CAMERA_NUM] = i_camPort
camera.outputs[2].format = mmal.MMAL_ENCODING_RGB24
camera.outputs[2].framesize = FULLSIZE_RES
camera.outputs[2].commit()
#camera.outputs[2].enable(image_callback)

encoderFull = mo.MMALImageEncoder()
encoderFull.inputs[0].format = mmal.MMAL_ENCODING_RGB24
encoderFull.inputs[0].framesize = FULLSIZE_RES
encoderFull.inputs[0].commit()
#encoderFull.outputs[0].copy_from(encoder.inputs[0])
encoderFull.outputs[0].format = mmal.MMAL_ENCODING_JPEG
encoderFull.outputs[0].params[mmal.MMAL_PARAMETER_JPEG_Q_FACTOR] = 95
encoderFull.outputs[0].commit()
if i_captureMode == "file":
    encoderFull.outputs[0].enable(image_encoder_full_file_callback)
elif i_captureMode == "port":
    encoderFull.outputs[0].enable(image_encoder_full_port_callback)

encoderFull.connect(camera.outputs[2])
encoderFull.connection.enable()

camera.enable()
encoderFull.enable()

"""
resizer = mo.MMALResizer()
resizer.connect(camera.outputs[2])
resizer.inputs[0].format = MMAL_ENCODING_RGB24
resizer.inputs[0].framesize = FULLSIZE_RES
resizer.outputs[0].format = MMAL_ENCODING_RGB24
resizer.outputs[0].framesize = HALFSIZE_RES

encoderHalf = mo.MMALImageEncoder()
encoderHalf.connect(resizer.outputs[0])
encoderHalf.connection.enable()
encoderHalf.inputs[0].format = mmal.MMAL_ENCODING_RGB24
encoderHalf.inputs[0].framesize = HALFSIZE_RES
encoderHalf.inputs[0].commit()
#encoderHalf.outputs[0].copy_from(encoder.inputs[0])
encoderHalf.outputs[0].format = mmal.MMAL_ENCODING_JPEG
encoderHalf.outputs[0].params[mmal.MMAL_PARAMETER_JPEG_Q_FACTOR] = 95
if i_captureMode == "file":
    encoderHalf.outputs[0].enable(image_encoder_half_file_callback)
elif i_captureMode == "port":
    encoderHalf.outputs[0].enable(image_encoder_half_port_callback)
encoderHalf.outputs[0].commit()
"""
sleep(0.1)

#cameraInfoMsg = camCalibProvider.parseCameraIntrinsicCalib(i_camInfoUrl)


rateNumber = float(1.0/i_imgPeriodTime)
rate = rospy.Rate(rateNumber)
while not rospy.is_shutdown():
    
    if i_captureMode == "file":
        capture_to_disk(path="/tmp/"+str(i_camPort))
    elif i_captureMode == "port":
        capture_to_port()
    
    #camera_info_pub.publish(cameraInfoMsg)
    rate.sleep()
