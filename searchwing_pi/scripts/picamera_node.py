#!/usr/bin/env python

import time
from datetime import datetime
import os
import math
import time
import sys
import shutil
from picamera import PiCamera
import numpy as np
from io import BytesIO

import rospy
from sensor_msgs.msg import CompressedImage, CameraInfo
from searchwing_common_py import camCalibProvider


class picamera_node():

    def __init__(self, cam_id, camName, camInfoUrl, camRotation):
        if cam_id == 0:
            led_pin=2
        if cam_id == 1:
            led_pin=30
        self.camera = PiCamera(camera_num=cam_id,led_pin=led_pin)
        self.camera.resolution = [3280, 2464]
        self.camera.exposure_mode = "auto"
        if camRotation == "forward":
            self.camera.vflip = False
            self.camera.hflip = False
        if camRotation == "backward":
            self.camera.vflip = True
            self.camera.hflip = True

        # Loop of exposure times for the cameras
        # 0 == auto, every other number is relativ exposure to the one before
        self.exposureLoopSettings = [0.0,0.0,0.25] 
        self.imageUniqueID = 0
        self.jpgQuality = 95

        self.cameraInfoMsg = camCalibProvider.parseCameraIntrinsicCalib(camInfoUrl)

        self.image_pub = rospy.Publisher('/'+camName+'/image/compressed', CompressedImage, queue_size=3)
        self.info_pub = rospy.Publisher('/'+i_camName+'/camera_info', CameraInfo, queue_size=3)

    def to_file(self,path):
        picStamp = datetime.now()
        filename = picStamp.strftime("%Y-%m-%dT%H-%M-%S.%f")
        jpg_file_full = os.path.join(path,'F_'+filename+'.jpg')
        os.makedirs(os.path.dirname(jpg_file_full), exist_ok=True)
        self.take_photo(jpg_file_full)
        print(jpg_file_full)

    def take_photo(self,filename):
        try:
            self.camera.capture(filename, format='jpeg',
                            quality=self.jpgQuality, thumbnail=None, use_video_port=False)
        except:
            rospy.logerr("Error while taking photo {}.".format(filename))

    def publish(self):
        exposureSettingCurrentIdx = self.imageUniqueID % len(self.exposureLoopSettings)
        exposureSettingCurrentIdxNext = (self.imageUniqueID+1) % len(self.exposureLoopSettings)
        exposureSetting = self.exposureLoopSettings[exposureSettingCurrentIdx]
        exposureSettingNext = self.exposureLoopSettings[exposureSettingCurrentIdxNext]

        if exposureSetting > 0:
            self.camera.shutter_speed = int( self.camera.exposure_speed * exposureSetting)
            self.camera.exposure_mode = 'off'
        else:
            self.camera.shutter_speed = 0  # set auto

        my_stream = BytesIO()

        imageStamp = rospy.Time.now()
        self.take_photo(my_stream)

        if exposureSetting != 0.0 and exposureSettingNext == 0.0:
            self.camera.shutter_speed = 0  # set auto
            self.camera.exposure_mode = "auto"
            # run a inital photo for automode without saving to disk to precalibrate the gains
            self.take_photo("/dev/shm/dummy")

        self.imageUniqueID = self.imageUniqueID + 1

        picMsg = CompressedImage()
        picMsg.header.stamp = imageStamp
        picMsg.format = "jpeg"
        picMsg.data = my_stream.getvalue()

        self.image_pub.publish(picMsg)
        cameraInfoMsg= self.cameraInfoMsg
        cameraInfoMsg.header.stamp = imageStamp
        self.info_pub.publish(self.cameraInfoMsg)


if __name__ == '__main__':
    rospy.init_node('picamera_node', anonymous=True)

    i_camPort  = rospy.get_param('~i_camPort',0)
    i_imgPeriodTime  = rospy.get_param('~i_imgPeriodTime',2.0)
    i_camName  = rospy.get_param('~i_camName',"picamera")
    i_camInfoUrl = rospy.get_param('~i_camera_info_url',"package://searchwing_common_py/config/bodenseeCamCalib.yaml")
    i_camRotation = rospy.get_param('~i_camera_rotation',"backward")

    p = picamera_node(i_camPort, i_camName, i_camInfoUrl, i_camRotation)

    rateNumber = float(1.0/i_imgPeriodTime)
    rate = rospy.Rate(rateNumber)
    while not rospy.is_shutdown():
        imageStamp=p.publish()
        rate.sleep()
