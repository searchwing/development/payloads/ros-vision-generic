#!/usr/bin/env python

import time
from datetime import datetime
import os
import math
import time
import sys
import shutil
import numpy as np
from io import BytesIO

from picamera2 import Picamera2
from libcamera import controls
from libcamera import Transform
from picamera2.encoders import JpegEncoder
from picamera2.controls import Controls

import rospy
from sensor_msgs.msg import CompressedImage, CameraInfo
from searchwing_common_py import camCalibProvider


class picamera2_node():

    def __init__(self, cam_id, camName, camInfoUrl, camRotation, jpegQuality):
        self.picam2 = Picamera2(cam_id)

        # config generic camera settings
        if camRotation == "forward":
            transform = Transform(hflip=0, vflip=0)
        if camRotation == "backward":
            transform = Transform(hflip=1, vflip=1)
        capture_config = self.picam2.create_still_configuration(
            transform=transform)  # full resolution
        self.picam2.configure(capture_config)
        self.picam2.options["quality"] = jpegQuality

        # config exposure
        """
        For details see:
        - tuningguide https://datasheets.raspberrypi.com/camera/raspberry-pi-camera-guide.pdf
        - tuningfiles e.g. /usr/share/libcamera/ipa/rpi/vc4/imx708.json
        """
        controls = Controls(self.picam2)
        """
        AE: Autoexposure 
        """
        controls.AeEnable = True
        """
        0: Normal - normal exposures
        1: Short - Exposure mode allowing only short exposure times. 
        2: Long - Exposure mode allowing long exposure times. 
        3: Custom - Custom exposure mode.
        """
        controls.AeExposureMode = 1
        """
        AE metering mode, 
        0: MeteringCentreWeighted - Centre-weighted metering mode.
        1: MeteringSpot - Spot metering mode.
        2: MeteringMatrix - Matrix metering mode, equal over whole image as defined in th tuning file.
        3: MeteringCustom - Custom metering mode. 
        """
        controls.AeMeteringMode = 2
        """
        Sets the constraint mode of the AEC/AGC algorithm
        0: Normal - his mode aims to balance the exposure of different parts of the image so as to reach a reasonable average level. However, highlights in the image may appear over-exposed and lowlights may appear under-exposed. 
        1: Highlight - This mode adjusts the exposure levels in order to try and avoid over-exposing the brightest parts (highlights) of an image. Other non-highlight parts of the image may appear under-exposed.
        2: Shadows - This mode adjusts the exposure levels in order to try and avoid under-exposing the dark parts (shadows) of an image. Other normally exposed parts of the image may appear over-exposed. 
        3: Custom - user-defined metering
        """
        controls.AeConstraintMode = 1
        self.picam2.set_controls(controls)
        # Loop of EV values for the camera images
        """
        By convention EV adjusts the exposure as log2. For example
        EV = [-2, -1, 0.5, 0, 0.5, 1, 2] results in an exposure adjustment
        of [1/4x, 1/2x, 1/sqrt(2)x, 1x, sqrt(2)x, 2x, 4x].
        """
        self.evLoopSettings = [0, -1, -2]
        self.imageUniqueID = 0

        # startup camera
        self.picam2.start()
        time.sleep(2)  # let AE get some time to adjust

        # get intrinsic camera calibration
        self.cameraInfoMsg = camCalibProvider.parseCameraIntrinsicCalib(
            camInfoUrl)

        # setup publishers
        self.image_pub = rospy.Publisher(
            '/'+camName+'/image/compressed', CompressedImage, queue_size=3)
        self.info_pub = rospy.Publisher(
            '/'+i_camName+'/camera_info', CameraInfo, queue_size=3)

    def to_file(self, path):
        picStamp = datetime.now()
        filename = picStamp.strftime("%Y-%m-%dT%H-%M-%S.%f")
        jpg_file_full = os.path.join(path, 'F_'+filename+'.jpg')
        os.makedirs(os.path.dirname(jpg_file_full), exist_ok=True)
        self.take_photo(jpg_file_full)
        print(jpg_file_full)

    def take_photo(self, filename):
        try:
            self.picam2.capture_file(filename, format='jpeg')
        except Exception as e:
            rospy.logerr(
                "Error while taking photo {}: {}".format(filename, str(e)))

    def publish_photo(self):
        # take photo and save to ram
        datastream = BytesIO()
        imageStamp = rospy.Time.now()
        self.take_photo(datastream)

        # update exposure / ev setting
        evSettingIdx = self.imageUniqueID % len(self.evLoopSettings)
        evSetting = self.evLoopSettings[evSettingIdx]
        controls = Controls(self.picam2)
        controls.ExposureValue = float(evSetting)
        self.picam2.set_controls(controls)
        self.imageUniqueID = self.imageUniqueID + 1

        # publish messages
        picMsg = CompressedImage()
        picMsg.header.stamp = imageStamp
        picMsg.format = "jpeg"
        picMsg.data = datastream.getvalue()
        self.image_pub.publish(picMsg)
        cameraInfoMsg = self.cameraInfoMsg
        cameraInfoMsg.header.stamp = imageStamp
        self.info_pub.publish(self.cameraInfoMsg)


if __name__ == '__main__':
    rospy.init_node('picamera2', anonymous=True)

    # get params
    i_camPort = rospy.get_param('~i_camPort', 0)
    i_imgPeriodTime = rospy.get_param('~i_imgPeriodTime', 2.0)
    i_camName = rospy.get_param('~i_camName', "picamera")
    i_camInfoUrl = rospy.get_param(
        '~i_camera_info_url', "package://searchwing_common_py/config/bodenseeCamCalib.yaml")
    i_camRotation = rospy.get_param('~i_camera_rotation', "backward")
    i_jpegQuality = rospy.get_param('~i_jpegQuality', 100)

    # start camera
    camera = picamera2_node(i_camPort, i_camName, i_camInfoUrl,
                       i_camRotation, i_jpegQuality)

    # take photos and publish
    rateNumber = float(1.0/i_imgPeriodTime)
    rate = rospy.Rate(rateNumber)
    while not rospy.is_shutdown():
        camera.publish_photo()
        rate.sleep()
